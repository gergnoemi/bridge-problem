#Gergely Noémi-Laura

import sys
import random

def build_from_file(file):
    x = []
    
    with open(file, "r") as f:
        x.extend([int(a.strip()) for a in f.readlines()])
    
    return x

def greedy_solver(x):
    left_side = x.copy()
    right_side = []
    time = 0
    solution = []

    while len(left_side) > 0:
        fastest = min(left_side)
        left_side.remove(fastest)
        slowest = max(left_side)
        left_side.remove(slowest)
        right_side.extend([fastest, slowest])
        solution.append((fastest, slowest))
        time += slowest

        if len(left_side) > 0:
            fastest = min(right_side)
            right_side.remove(fastest)
            left_side.append(fastest)
            time += fastest
            solution.append(tuple([fastest]))
    
    return time, solution   

def random_solver(x):
    left_side = x.copy()
    right_side = []
    time = 0
    solution = []

    while len(left_side) > 0:
        first_person, second_person = random.sample(left_side, 2)
        left_side.remove(first_person)
        left_side.remove(second_person)
        time += max([first_person, second_person])
        right_side.extend([first_person, second_person])
        solution.append((first_person, second_person))

        if len(left_side) > 0:
            person = random.sample(right_side, 1)[0]
            right_side.remove(person)
            left_side.append(person)
            time += person
            solution.append(tuple([person]))

    return time, solution

def mutation(original, x):
    chromosome = original.copy()
    twice_walkers = [i for i in chromosome if len(i) == 1]
    gene = random.sample(twice_walkers, 1)[0][0]

    left_side = x.copy()
    right_side = []
    found_gene = False
    i = -1
    while not found_gene:
        i += 1
        current_gene = chromosome[i]
        if len(current_gene) == 2:
            left_side.remove(current_gene[0])
            left_side.remove(current_gene[1])
            right_side.extend(current_gene)
        else:
            right_side.remove(current_gene[0])
            left_side.append(current_gene[0])
        if gene in current_gene:
            found_gene = True

    index_at_departure = i
    left_at_departure = left_side.copy()
    right_at_departure = right_side.copy()

    petrified_ones = left_side.copy()
    
    found_gene = False
    while not found_gene:
        i += 1
        current_gene = chromosome[i]
        if len(current_gene) == 2:
            left_side.remove(current_gene[0])
            left_side.remove(current_gene[1])
            right_side.extend(current_gene)
            if current_gene[0] in petrified_ones:
                petrified_ones.remove(current_gene[0])
            if current_gene[1] in petrified_ones:
                petrified_ones.remove(current_gene[1])
        else:
            right_side.remove(current_gene[0])
            left_side.append(current_gene[0])
            
            if gene in current_gene:
                found_gene = True

    if len(petrified_ones) > 0:
        random_gene = random.sample(petrified_ones,1)[0]

        #luigi
        new_gene = ()

        if chromosome[index_at_departure][0] == gene:
            new_gene = (random_gene, chromosome[index_at_departure][1])
        else:
            new_gene = (chromosome[index_at_departure][0], random_gene)
        chromosome[index_at_departure] = new_gene
        chromosome[i] = tuple([random_gene])

    else:
        subchromosome = [j for j in chromosome if len(j) == 1]
        random_gene = random.sample(subchromosome, 1)[0]
        index = chromosome.index(random_gene)
        previous_gene = chromosome[index-1]
        next_gene = chromosome[index+1]
        
        first = None
        into_first = None
        second = None
        into_second = None
        if random_gene[0] == previous_gene[0]:
            into_second = previous_gene[1]
            first = previous_gene[0]
        else:
            into_second = previous_gene[0]
            first = previous_gene[1]
        if random_gene[0] == next_gene[0]:
            into_first = next_gene[1]
            second = next_gene[0]
        else:
            into_first = next_gene[0]
            second = next_gene[1]

        first_gene = (first, into_first)
        second_gene = (second, into_second)
        chromosome[index-1] = first_gene
        chromosome[index+1] = second_gene
    
    return chromosome

def crossover(parent1, parent2):

    subparent1 = [j for j in parent1 if len(j) == 1]
    subparent2 = [j for j in parent2 if len(j) == 1]
    fastest1 = min(subparent1)[0]
    fastest2 = min(subparent2)[0]
    child1 = parent1.copy()
    child1.append(tuple([-1]))
    child2 = parent2.copy()
    child2.append(tuple([-1]))

    n = len(child1)
    if fastest1 != fastest2:
        for even, odd in zip(range(0,n,2), range(1,n,2)):
            even_gene1 = child1[even]
            even_gene2 = child2[even]
            odd_gene1 = child1[odd]
            odd_gene2 = child2[odd]

            #even_gene1
            if even_gene1[0] == fastest1:
                if even_gene1[1] == fastest2:
                    pass
                else:
                    child1[even] = tuple([fastest2, even_gene1[1]])
                    
            elif even_gene1[0] == fastest2:
                if even_gene1[1] == fastest1:
                    pass
                else:
                    child1[even] = tuple([fastest1, even_gene1[1]])
                    
            elif even_gene1[1] == fastest1:
                if even_gene1[0] == fastest2:
                    pass
                else:
                    child1[even] = tuple([fastest2, even_gene1[0]])

            elif even_gene1[1] == fastest2:
                if even_gene1[0] == fastest1:
                    pass
                else:
                    child1[even] = tuple([even_gene1[0], fastest1])


            #even_gene2
            if even_gene2[0] == fastest1:
                if even_gene2[1] == fastest2:
                    pass
                else:
                    child2[even] = tuple([fastest2, even_gene2[1]])
                    
            elif even_gene2[0] == fastest2:
                if even_gene2[1] == fastest1:
                    pass
                else:
                    child2[even] = tuple([fastest1, even_gene2[1]])
                    
            elif even_gene2[1] == fastest1:
                if even_gene2[0] == fastest2:
                    pass
                else:
                    child2[even] = tuple([fastest2, even_gene2[0]])
                    
            elif even_gene2[1] == fastest2:
                if even_gene2[0] == fastest1:
                    pass
                else:
                    child2[even] = tuple([even_gene2[0], fastest1])

            #odd_gene1
            if odd_gene1[0] == fastest1:
                child1[odd] = tuple([fastest2])
            elif odd_gene1[0] == fastest2:
                child1[odd] = tuple([fastest1])

            #odd_gene2
            if odd_gene2[0] == fastest1:
                child2[odd] = tuple([fastest2])
            elif odd_gene2[0] == fastest2:
                child2[odd] = tuple([fastest1])
            
    return child1[:-1], child2[:-1]

def fitness(x):
    sum = 0
    for i in x:
        sum += max(i)

    return sum

def genetic_solver(x):
    n = 50 #size of population
    random_times = []
    random_solutions = []
    gen = 10000 #number of generations
    mp = 0.15 #probability of mutation
    cp = 0.7 #probability of crossover

    #generate a random initial solution
    for _ in range(0, n):
        random_time, random_solution = random_solver(x)
        random_times.append(random_time)
        random_solutions.append(random_solution)

    #repeat for number of generations
    for i in range(0, gen):
        all = []
        mutations = []
        children = []
        #mutation
        for j in range(0, n):
            #mutate current individual with a probability
            if random.random() < mp:
                entity = mutation(random_solutions[j], x)
                mutations.append(entity)
        for j in range(0, n):
            #crossover two random individuals with a probability
            if random.random() < cp:
                parent1, parent2 = random.sample(random_solutions,2)
                child1, child2 = crossover(parent1, parent2)
                children.extend([child1, child2])
        all.extend(random_solutions)
        all.extend(mutations)
        all.extend(children)
        #sort individuals by fitness function
        all.sort(key=fitness)
        #select the best n individuals
        random_solutions = all[:n]

    random_solutions.sort(key=fitness)
    print(random_solutions[0])
    print(fitness(random_solutions[0]))
    #print([fitness(i) for i in random_solutions])
    
def print_solution(time, solution, solving_method):
    print("Method used:", solving_method)
    print("The people got to the other side of the bridge in", time, "minutes\n")
    print("Order of walks back and forth: ", solution)

def main():
    if len(sys.argv) != 2:
        print("Invalid argument")
        sys.exit(1)
        
    x = build_from_file(sys.argv[1])
    time, solution = greedy_solver(x)
    print_solution(time, solution, "GREEDY SOLVER")

    genetic_solver(x)

if __name__ == "__main__":
    main()