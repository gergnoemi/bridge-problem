### Optimization problems
## Genetic algorithms
# The Bridge Problem

The bridge and torch problem is a logic puzzle that originally deals with four people, a bridge and a torch. It is one of the category of river crossing puzzles, where a number of objects must move across a river, with some constraints.
Several variations exist, with cosmetic variations such as differently named people, or variation in the crossing times, number of people or time limit. The torch itself may expire in a short time and so serve as the time limit.


### **So what is the story?**

A group of people come to a river in the night and they want to get to the other side. There is a narrow bridge, but it can only hold two people at a time. They have one torch and, because it's night, the torch has to be used when crossing the bridge. For each person it takes a different amount of time to cross the bridge, varying from one minute to even twenty minutes. When two people cross the bridge together, they must move at the slower person's pace. The question is, how long does it take for all to get across the bridge?

---
### **Solution**
For solving the problem, we use a **genetic algorithm**.

A genetic algorithm (GA) is a metaheuristic inspired by the process of natural selection that belongs to the larger class of evolutionary algorithms (EA). Genetic algorithms are commonly used to generate high-quality solutions to optimization and search problems by relying on bio-inspired operators such as **mutation, crossover** and **selection**.

In the genetic algorithm, a population of candidate solutions (called individuals, creatures, or phenotypes) to an optimization problem is evolved toward better solutions. Each candidate solution has a set of properties (its **chromosomes** or genotype) which can be mutated and altered; traditionally, solutions are represented in binary as strings of 0s and 1s, but other encodings are also possible. 

In the problem presented above, the population is represented by **a number of random solutions**, each individual (entity) being **one solution** represented by a chromosome: an array storing **the walks made on the bridge in order** (back and forth). The chromosome is made of genes, each gene being **one walk**, either to the left or the right side of the bridge, represented by one or two people making the walk.

**Mutation** of an individual is made by switching person A with person B, where person A at some time makes both departure and return to the initial side of the bridge while person B doesn’t move from the moment A leaves to the moment of their arrival. If no such case exists, we simply find a gene where someone makes a return, and we switch the people who leave in the genes right before and after it.

**Crossover** is achieved by finding the fastest person who makes a return in each of two solutions (parents) and switching their occurrence in each gene of the two individuals.

**Selection** is then made by selecting the best individuals from the current population, the mutated individuals and the children using the **fitness function**, that being the total amount of time of the walks on the bridge.

---

### **Parameters** of the algorithm
```python
def genetic_solver(x):
    n = 50 #size of population
    gen = 10000 #number of generations
    mp = 0.15 #probability of mutation
    cp = 0.7 #probability of crossover
```

---

### **Results** of comparison between evolutionary and greedy methods
Test cases:
1. Input: 5 6 7 8\
Results: 31 31 31 31 31 31 31 31 31 31\
Average result using genetic algorithm: 31\
Result using greedy algorithm:  31\
Deviation: 0

2. Input: 1 2 5 10\
Results: 17 17 19 17 19 17 19 19 19 17\
Average result using genetic algorithm: 17.8\
Result using greedy algorithm: 19\
Deviation: 1

3. Input: 4 1 3 12 5 10\
Results: 35 35 35 37 37 40 40 41 39 39\
Average result using genetic algorithm: 37.8\
Result using greedy algorithm: 38\
Deviation: 2.18

---

### **Conclusion**
The genetic algorithm on average will return a (slightly) better result than the greedy algorithm, but it will be a lot more time consuming.

---

2019 UBB, Gergely Noémi-Laura.
